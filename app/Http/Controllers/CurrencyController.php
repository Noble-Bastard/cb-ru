<?php

namespace App\Http\Controllers;

use Domain\Currency\Contracts\CurrencyHistoryServiceInterface;
use Domain\Currency\Contracts\CurrencyServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CurrencyController extends Controller
{

    public function __construct(
        private readonly CurrencyServiceInterface        $currencyService,
        private readonly CurrencyHistoryServiceInterface $currencyHistoryService
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->currencyService->getCurrentCurrency();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function historyAll(): Response
    {
        return $this->currencyHistoryService->getAllHistory();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function findByHistory(Request $request): Response
    {
        // TODO Validation
        return $this->currencyHistoryService->getCurrencyOfDate($request->post('date'));
    }

}
