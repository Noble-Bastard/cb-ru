<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Domain\Currency\Contracts\CurrencyHistoryServiceInterface;
use Domain\Currency\Contracts\CurrencyServiceInterface;
use Domain\Currency\DTO\CurrencyDTO;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;

class UpdateCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param CurrencyServiceInterface $currencyService
     * @param CurrencyHistoryServiceInterface $currencyHistoryService
     */
    public function __construct(private readonly CurrencyServiceInterface $currencyService, private readonly CurrencyHistoryServiceInterface $currencyHistoryService)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $currencies = $this->currencyService->getCurrentCurrency();
            foreach ($currencies['Valute'] as $currency) {
                $this->currencyHistoryService->setCurrency(
                    new CurrencyDTO(
                        $currency['Name'],
                        $currency['ID'],
                        $currency['NumCode'],
                        $currency['CharCode'],
                        $currency['Nominal'],
                        $currency['Value'],
                        $currency['Previous'],
                        new Carbon($currencies['Date'])
                    ));
            }
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
        return 0;
    }
}
