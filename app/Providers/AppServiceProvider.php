<?php

namespace App\Providers;

use Domain\Currency\Contracts\CurrencyGatewayInterface;
use Domain\Currency\Contracts\CurrencyHistoryServiceInterface;
use Domain\Currency\Contracts\CurrencyRepositoryInterface;
use Domain\Currency\Contracts\CurrencyServiceInterface;
use Domain\Currency\Services\CurrencyHistoryService;
use Domain\Currency\Services\CurrencyService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Persistance\Currency\Gateways\CurrencyGateway;
use Infrastructure\Persistance\Currency\Repositories\CurrencyRepository;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CurrencyGatewayInterface::class, CurrencyGateway::class);
        $this->app->bind(CurrencyHistoryServiceInterface::class, CurrencyHistoryService::class);
        $this->app->bind(CurrencyRepositoryInterface::class, CurrencyRepository::class);
        $this->app->bind(CurrencyServiceInterface::class, CurrencyService::class);
    }
}
