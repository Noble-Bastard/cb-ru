<?php

namespace Infrastructure\Persistance\Currency\Gateways;

use Domain\Currency\Contracts\CurrencyGatewayInterface;
use http\Env;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CurrencyGateway implements CurrencyGatewayInterface
{

    private const URL = '/daily_json.js';

    public function __construct(protected readonly Http $http, protected readonly string $baseUrl = 'https://www.cbr-xml-daily.ru')
    {
    }

    /**
     * @return Response
     */
    public function createInvoice(): Response
    {
        $response = $this->get(self::URL);
        if ($response->status() != 200) {
            throw new HttpException($response->status(), 'Not 200');
        }

        return $response;
    }

    protected function get(string $url): Response
    {
        return Http::get($this->baseUrl . $url);
    }
}
