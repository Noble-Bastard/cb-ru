<?php

namespace Infrastructure\Persistance\Currency\Repositories;

use App\Models\Currency;
use Carbon\Carbon;
use Domain\Currency\Contracts\CurrencyRepositoryInterface;
use Domain\Currency\DTO\CurrencyDTO;

class CurrencyRepository implements CurrencyRepositoryInterface
{

    public function findByDate($date): CurrencyDTO|null
    {
        $currency = Currency::query()->whereDate('created_at', '=', new Carbon($date));
        if ($currency != null) {
            return new CurrencyDTO(
                $currency->name,
                $currency->currency_id,
                $currency->num_code,
                $currency->char_code,
                $currency->nominal,
                $currency->value,
                $currency->prev_value,
                $currency->created_at);
        }
        return null;
    }

    /**
     * List of Currency[]
     **/
    public function getAll(): array
    {
        return Currency::all();
    }

    public function set(CurrencyDTO $dto): void
    {
        Currency::query()->create([
            'name' => $dto->name,
            'currency_id' => $dto->currencyId,
            "num_code" => $dto->numCode,
            "char_code" => $dto->charCode,
            "nominal" => $dto->nominal,
            "value" => $dto->value,
            "previous_value" => $dto->prevValue
        ]);
    }
}
