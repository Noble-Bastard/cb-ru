<?php

namespace Domain\Currency\Services;

use Domain\Currency\Contracts\CurrencyHistoryServiceInterface;
use Domain\Currency\Contracts\CurrencyRepositoryInterface;
use Domain\Currency\DTO\CurrencyDTO;

class CurrencyHistoryService implements CurrencyHistoryServiceInterface
{

    public function __construct(private readonly CurrencyRepositoryInterface $currencyRepository)
    {
    }

    /**
     * @return mixed
     */
    public function getAllHistory(): array
    {
        return $this->currencyRepository->getAll();
    }

    /**
     * @param $date
     * @return CurrencyDTO|null
     */
    public function getCurrencyOfDate($date): ?CurrencyDTO
    {
        return $this->currencyRepository->findByDate($date);
    }

    /**
     * @param CurrencyDTO $dto
     * @return void
     */
    public function setCurrency(CurrencyDTO $dto): void
    {
      $this->currencyRepository->set($dto);
    }
}
