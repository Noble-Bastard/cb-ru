<?php

namespace Domain\Currency\Services;

use Domain\Currency\Contracts\CurrencyGatewayInterface;
use Domain\Currency\Contracts\CurrencyServiceInterface;

class CurrencyService implements CurrencyServiceInterface
{
    public function __construct(
        private readonly CurrencyGatewayInterface $currencyGateway
    )
    {
    }

    public function getCurrentCurrency()
    {
        return $this->currencyGateway->createInvoice();
    }

}
