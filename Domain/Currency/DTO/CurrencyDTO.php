<?php

namespace Domain\Currency\DTO;
use Carbon\Carbon;

class CurrencyDTO
{

    public function __construct(
        public readonly string $name,
        public readonly string $currencyId,
        public readonly string $numCode,
        public readonly string $charCode,
        public readonly int $nominal,
        public readonly float $value,
        public readonly float $prevValue,
        public readonly Carbon $date
    )
    {
    }
}
