<?php

namespace Domain\Currency\Contracts;

use Domain\Currency\DTO\CurrencyDTO;

interface CurrencyRepositoryInterface
{
    public function findByDate($date): CurrencyDTO|null;

    public function getAll(): array;

    public function set(CurrencyDTO $dto): void;
}
