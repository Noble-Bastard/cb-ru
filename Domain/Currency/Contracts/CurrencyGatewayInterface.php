<?php
namespace Domain\Currency\Contracts;
interface CurrencyGatewayInterface
{
    public function createInvoice();
}
