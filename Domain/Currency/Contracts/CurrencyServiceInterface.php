<?php
namespace Domain\Currency\Contracts;
interface CurrencyServiceInterface
{
    public function getCurrentCurrency();
}
