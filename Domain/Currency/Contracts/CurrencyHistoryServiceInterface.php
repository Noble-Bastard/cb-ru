<?php

namespace Domain\Currency\Contracts;
use Domain\Currency\DTO\CurrencyDTO;

interface CurrencyHistoryServiceInterface
{
    public function getAllHistory();

    public function getCurrencyOfDate($date);
    public function setCurrency(CurrencyDTO $dto);


}
