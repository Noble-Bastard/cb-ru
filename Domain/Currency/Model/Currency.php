<?php

namespace Domain\Currency\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "currency_id",
        "num_code",
        "char_code",
        "nominal",
        "value",
        "previous_value"
    ];
}
