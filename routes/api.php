<?php

use App\Http\Controllers\CurrencyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    // Authenticated Routes in api.php
    Route::get('/current', [CurrencyController::class, 'index']);
    Route::get('/history/all', [CurrencyController::class, 'historyAll']);
    Route::post('/history/by-date', [CurrencyController::class, 'findByHistory']);
});
